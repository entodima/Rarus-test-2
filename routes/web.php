<?php

setlocale(LC_ALL, "ru_RU.UTF-8");
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function (){
    return redirect(route('feedback'));
});

Route::get('/feedback', function (){
    return view('index');
})->name('feedback');
