<?php

namespace App\Http\Controllers;

use App\Feedback;
use Illuminate\Http\Request;

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

class FeedbackRestController extends Controller
{
    public function index() {
       return Feedback::all();
    }

    public function create() {
        //
    }

    public function store(Request $request)
    {
        $re_name = "/^([А-Я]{1}[а-яё]{1,23}|[A-Z]{1}[a-z]{1,23}) ([А-Я]{1}[а-яё]{1,23}|[A-Z]{1}[a-z]{1,23}) ([А-Я]{1}[а-яё]{1,23}|[A-Z]{1}[a-z]{1,23})$/um";
        $re_phone = '/^(7{1}[0-9]{1,10})$/mu';
        $errors = [];
        $success = [];

        try{
            $name = $_POST['name'];
            $phone = $_POST['phone'];
        } catch (\ErrorException $exception) {
            abort(400,'Имя и телефон обязательны к заполению');
        }

        if (!preg_match($re_name, $name)) array_push($errors,'Неправильно указано имя'); //abort(400, 'Неправильно указано имя');
        if (!preg_match($re_phone,$phone)) array_push($errors,'Формат номера: 7xxxxxxxxxx'); //abort(400, 'Формат номера: 7xxxxxxxxxx');

        @$email = $_POST['email'];
        @$address = $_POST['address'];
        @$comment = $_POST['comment'];
        @$file = $_FILES['file'];

        if (isset($email)) {
            $re_email = '/^([а-яА-Яa-zA-Z0-9_.+-]+@[а-яА-Яa-zA-Z0-9-]+\.[а-яА-Яa-zA-Z0-9-.]+)$/mu';
            $re_gmail = '/^([а-яА-Яa-zA-Z0-9_.+-]+@gmail+\.com+)$/mu';
            if (!preg_match($re_email, $email)) array_push($errors,'Неправильно указан email'); //abort(400, 'Неправильно указан email');
            else if (preg_match($re_gmail, $email)) array_push($errors,'Регистрация пользователей на Gmail невозможна!');// abort(400, 'Регистрация пользователей с таким почтовым адресом невозможна');
        }

        if (count($errors)==0){
            $feedback= new Feedback;
            $feedback->name = $name;
            $feedback->phone = $phone;
            if(isset($email)) $feedback->email = $email;
            if(isset($address)) $feedback->address = $address;
            if(isset($comment)) $feedback->comment = $comment;
            if(isset($file)) $feedback->file = $file['name'];
            if (!$feedback->save()) array_push($errors,'Ошибка сохранения');
            else array_push($success,'Отзыв успешно добавлен');
        }

        $logger = new Logger('DEBUG LOGGER');
        $logger->pushHandler(new StreamHandler(storage_path('logs/feedback.log'),Logger::DEBUG));

        $logger->debug('Заполенение формы', array('name'=>$name, 'phone'=>$phone, 'email'=>$email, 'address'=>$address, 'comment'=>$comment));

        return response()->json([
            'errors'=>$errors,
            'success'=>$success
        ]);
    }

    public function show($id)  {
        //
    }

    public function edit($id) {
        //
    }

    public function update(Request $request, $id) {

    }

    public function destroy($id) {
        //
    }
}
