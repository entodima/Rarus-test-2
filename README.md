# Задача №2 — «Форма обратной связи»

[Компонент Vue](https://github.com/entodima/Rarus-test-2/blob/master/resources/js/components/FeedbackComponent.vue)

[API](https://github.com/entodima/Rarus-test-2/blob/master/app/Http/Controllers/FeedbackRestController.php)

[Лог](https://github.com/entodima/Rarus-test-2/blob/master/storage/logs/feedback.log)

[Таблица БД](https://github.com/entodima/Rarus-test-2/blob/master/database/migrations/2019_07_14_095515_create_feedback_table.php)

<hr>

[Задание](https://github.com/rarus/intern-php-developer/blob/master/task02-feedback-form.md)
